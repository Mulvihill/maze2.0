var ss = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
let gGameBoard = document.querySelector('#gameMap');
function splitMap(badMap) {
    let newMap = [];
    for (i=0;i<15;i++) {
        newMap.push(badMap[i].split(''));
        console.log(i);
        console.log(newMap);
    }
    return newMap;
}
let map = splitMap(ss);

function initMap (someArr) {
    for (k = gGameBoard.childElementCount; k != 0; k--) {gGameBoard.removeChild(gGameBoard.lastElementChild)};
    for (row in someArr) {
        const getMapBody = document.getElementById('gameMap');
        const initRow = document.createElement('div');
        initRow.id = 'row';
        initRow.dataset.rowCoordinate = row;
        getMapBody.appendChild(initRow);
        for (cell in someArr[row]) {
            const newCell = document.createElement('div');
            newCell.dataset.cellCoordinate = row + "-" + cell;
            newCell.id = 'block';
            newCell.classList.add(cellType(someArr[row][cell]));
            initRow.appendChild(newCell);
        }
    }

};

function cellType(mapStr) {
    if (mapStr === " ") {
        var type = 'empty';
    }
    else if (mapStr === "W") {
        var type = 'wall';
    }
    else if (mapStr === "S") {
        var type = 'player';
    }
    else if (mapStr === "F") {
        var type = 'finish'
    }
    return type;
};

function navigation(directionStr, coordinateStr, dubArr) {
    let coordArr = coordinateStr.split('-');
    coordArr[0] = Number(coordArr[0]);
    coordArr[1] = Number(coordArr[1]);
    if (directionStr == 'ArrowRight') {
        let destination = dubArr[coordArr[0]][coordArr[1]+1];
        if (destination != 'W'&& destination != null){
            if(destination == 'F') { alert("Winner!")}
            else{
                dubArr[coordArr[0]][coordArr[1]] = ' ';
                dubArr[coordArr[0]][coordArr[1]+1] = 'S';
                console.log(dubArr);
                initMap(dubArr);
            }
        }
        else {alert('Invalid move')};
    }
    else if (directionStr == 'ArrowLeft') {
        let destination = dubArr[coordArr[0]][coordArr[1]-1];
        if (destination != 'W'&& destination != null){
            if(destination == 'F') { alert("Winner!")}
            else{
                dubArr[coordArr[0]][coordArr[1]] = ' ';
                dubArr[coordArr[0]][coordArr[1]-1] = 'S';
                initMap(dubArr);
            }
        }
        else {alert('Invalid move')};
    }
    else if (directionStr == 'ArrowUp') {
        let destination = dubArr[coordArr[0]-1][coordArr[1]];
        if (destination != 'W'&& destination != null){
            if(destination == 'F') { alert("Winner!")}
            else{
                dubArr[coordArr[0]][coordArr[1]] = ' ';
                dubArr[coordArr[0]-1][coordArr[1]] = 'S';
                initMap(dubArr);
            }
        }
        else {alert('Invalid move')};
    }
    else if (directionStr == 'ArrowDown') {
        let destination = dubArr[coordArr[0]+1][coordArr[1]];
        if (destination != 'W'&& destination != null){
            if(destination == 'F') { alert("Winner!")}
            else{
                dubArr[coordArr[0]][coordArr[1]] = ' ';
                dubArr[coordArr[0]+1][coordArr[1]] = 'S';
                initMap(dubArr);
            }
        }
        else {alert('Invalid move')};
    }
    else {alert("Invalid move")}

}


function movePiece(key) {
    console.log(key.code);
    const currentPosition = document.querySelector('.player').dataset.cellCoordinate;
    navigation(key.code, currentPosition, map);
}
initMap(map);
document.addEventListener('keydown', movePiece);
